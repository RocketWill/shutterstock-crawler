# Shutterstock Crawler

## Requirements
1. 
```bash
pip install beautifulsoup4
pip install selenium
pip install lxml
```
2. Install [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads). ChromeDriver 版本需与 Chrome 版本相同 ([查询Chrome版本](https://www.whatismybrowser.com))
3. 将 ChromeDriver 位置更新至 shutterscrape.py

## Usage
```bash
python shutterscrape.py [output_dir] [terms] [-type] [-page] [-larger]
```

### 参数说明
1. `output_dir`: 图片输出文件夹
2. `terms`: 搜索关键字，若有多个关键字请用 `+` 号连接，示例： `people+use+mobile`
3. `-type`: 搜索类型，默认为 `all`，若只要照片可以使用 `p` 或 `photp`
4. `-page`: 欲搜索的页数，默认为 `1` 
5. `-larger`: 是否下载大图（会有watermark），例如 `-larger y`