from argparse import ArgumentParser
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import os
import ssl
from urllib.request import urlretrieve

ssl._create_default_https_context = ssl._create_unverified_context

def imagescrape(chrome_driver_path, searchPage, searchTerm, image_type, scrape_directory, large):
    try:
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        driver = webdriver.Chrome(executable_path=chrome_driver_path)
        driver.maximize_window()
        for i in range(1, searchPage+1):
        #for i in range(1, 29):
            url = "https://www.shutterstock.com/search?searchterm=" + searchTerm + "&sort=popular&image_type=" + image_type + "&search_source=base_landing_page&language=en&page=" + str(
                i)
            driver.get(url)
            data = driver.execute_script("return document.documentElement.outerHTML")
            print("Page " + str(i))
            scraper = BeautifulSoup(data, "lxml")
            img_container = scraper.find_all("img", {"class": "z_h_c z_h_e"})
            print(img_container)
            for j in range(0, len(img_container) - 1):
                img_src = img_container[j].get("src")
                if large:
                    img_src = img_src.replace("image-photo", "z")
                    img_src = img_src.replace("260nw-", "")
                name = img_src.rsplit("/", 1)[-1]
                try:
                    urlretrieve(img_src, os.path.join(scrape_directory, os.path.basename(img_src)))
                    print("Scraped " + name)
                except Exception as e:
                    print(e)
        driver.close()
    except Exception as e:
        print(e)
    print('Finished!')

if __name__ == "__main__":

    chrome_driver_path = "/Users/willc/Downloads/chromedriver"

    parser = ArgumentParser()
    parser.add_argument("output_dir", help="image output directory")
    parser.add_argument("terms", help="search terms")
    parser.add_argument("-type", default='all', help="")
    parser.add_argument("-page", default=1, help="")
    parser.add_argument("-large", default=0, help="")
    args = parser.parse_args()

    type = 'all'
    if args.type.lower() in ['p', 'photo']:
        type = 'photo'

    page = 1
    if int(args.page) > 1:
        page = int(args.page)

    large = False
    if args.large:
        large = True

    os.makedirs(args.output_dir, exist_ok=True)

    imagescrape(chrome_driver_path, page, args.terms, type, args.output_dir, large)